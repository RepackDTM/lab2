# Название 
Сбор и аналитическая обработка информации о сетевом трафике

## Цель
1. Развить практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетвом трафике
2. Освоить базовые подходы блокировки нежелательного сетевого трафика
3. Закрепить знания о современных сетевых протоколах прикладного уровня

## Исходные данные
1. ПК на базе Windows
2. Программа Wireshark
3. Виртуальная машина KALI Linux с утилитой ZEEK
4. Python 

## План решения задачи. 

## 


1. Собрать сетевой трафик (объемом не менее 100 Мб)
2. Выделить метаинформацию сетевого трафика с помощью утилиты Zeek
3. Собрать данные об источниках нежелательного трафика (например – https://github.com/StevenBlack)
4. Написать программу на любом удобном языке (python, bash,), котрая подсчитывает процент собранного неже лательного трафика.



# Содержание ЛБ
## Шаг 1 Собрать сетевой трафик (объемом не менее 100 Мб)
Сбор траффика с интерфейса ethernet
![](photo/Screenshot_4.png)

Сбор происходит в то время пока мы сёрфим в интернете
 ![](photo/Screenshot_6.png)


Сохраняем файл

## Шаг 2 Выделить метаинформацию с помощью утилиты Zeek
 На виртуальной машине запускаем KALI Linux и утилиту ZEEK

![](photo/Screenshot_1.png)

Получаем распределённые данные о трафике

![](photo/Screenshot_2.png)

## Шаг 3 Собрать данные об источниках нежелательного трафика

Скопируем нежелательные источники с https://github.com/StevenBlack
![](photo/Screenshot_7.png)

## Шаг 4 Написать програму на любом удобном языке (python, bash…), котрая подсчитывает процент нежелательного трафика в собранном на этапе 1.
```
import csv
import re

dnslog = open("dns1.log")
traff = open('uwhosts.txt')
dnstraff = csv.reader(dnslog, delimiter="\x09")
count = 0
unwcount = 0
reg = re.compile('[^a-zA-Z.]')
hosts=list()

for line in traff.readlines():
	hosts.append(reg.sub('', line[8:]))

for row in dnstraff:
	try:
		count+=1
		if row[9] in hosts:
			print(row[9])
			unwcount+=1
	except IndexError:
		pass
res = (unwcount/count)*100
print("unwanted traffic = "+str(res) + "%")


dnslog.close()
traff.close()

```
## Результат
![](photo/Screenshot_5.png)

## Вывод
При выполнении практической работы были развиты практические навыки использования Zeek и Wireshark. Также были освоены навыки по анализу и сортировке нежелательного трафика.

